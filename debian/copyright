Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dune-localfunctions
Source: https://www.dune-project.org/dev/downloadgit/

Files: *
Copyright:
 2015--2016    Marco Agnese
 2008--2010    Peter Bastian
 2015          Timo Betcke
 2009--2014    Markus Blatt
 2016          Lukas Böger
 2014          Andreas Buhr
 2014--2017    Ansgar Burchardt
 2009--2021    Andreas Dedner
 2020          Nils-Arne Dreier
 2010          Martin Drohmann
 2008--2020    Christian Engwer
 2008--2020    Jorrit Fahlke
 2011--2013    Bernd Flemisch
 2015          Elisa Friebel
 2012          Christoph Gersbacher
 2019          Janick Gerstenberger
 2009--2021    Carsten Gräser
 2015--2018    Felix Gruber
 2011--2021    Christoph Grüninger
 2020          René Heß
 2017          Patrick Jaap
 2013          Guillaume Jouvet
 2015--2020    Dominic Kempf
 2015          Angela Klewinghaus
 2020          Robert Klöfkorn
 2014          Arne Morten Kvarving
 2015          Jizhou Li
 2013--2017    Tobias Malkmus
 2009          Sven Marnach
 2013--2018    Steffen Müthing
 2020          Lisa Julia Nebel
 2012          Rebecca Neumann
 2008--2018    Martin Nolte
 2011--2016    Elias Pipping
 2016--2021    Simon Praetorius
 2012--2013    Human Rezaijafari
 2011--2012    Uli Sack
 2008--2021    Oliver Sander
 2020--2021    Henrik Stolzmann
 2011--2012    Matthias Wohlmuth
 2010--2015    Jonathan Youett
License: GPL-2 with DUNE exception

Files: debian/*
Copyright: 2011-2020, Ansgar <ansgar@debian.org>
License: GPL-2 with DUNE exception

License: GPL-2 with DUNE exception
 The DUNE library and headers are licensed under version 2 of the GNU General
 Public License, with a special exception for linking and compiling against
 DUNE, the so-called "runtime exception." The license is intended to be
 similar to the GNU Lesser General Public License, which by itself isn't
 suitable for a template library.
 .
 The exact wording of the exception reads as follows:
 .
 As a special exception, you may use the DUNE library without
 restriction.  Specifically, if other files instantiate templates or
 use macros or inline functions from one or more of the DUNE source
 files, or you compile one or more of the DUNE source files and link
 them with other files to produce an executable, this does not by
 itself cause the resulting executable to be covered by the GNU
 General Public License.  This exception does not however invalidate
 any other reasons why the executable file might be covered by the
 GNU General Public License.
 .
 This license clones the one of the libstdc++ library.
 .
 On Debian systems, the full text of the GNU General Public License version 2
 can be found in the file  `/usr/share/common-licenses/GPL-2'.
